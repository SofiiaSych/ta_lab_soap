package com.epam.requests;

import com.epam.models.Author;

import javax.xml.soap.SOAPMessage;

public class AuthorRequest extends BaseRequest{

    public SOAPMessage createAuthorRequest(int authorId) {
        Author author = new Author(authorId, new Author.Name("Arturo", "Roman"), "Spanish",
                new Author.Birth("1972-02-13", "Spain", "Madrid"), "Some descriptions");
        return new RequestBuilder("createAuthorRequest")
                .createAuthor(author)
                .build();
    }

    public SOAPMessage getAuthorRequest(int authorId) {
        return new RequestBuilder("getAuthorRequest")
                .addAuthorId(authorId)
                .build();
    }

    public SOAPMessage deleteAuthorRequest(int authorId) {
        return new RequestBuilder("deleteAuthorRequest")
                .deleteAuthor(authorId, false)
                .build();
    }

}
