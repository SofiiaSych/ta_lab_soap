package com.epam.requests;

import com.epam.models.Author;
import javax.xml.soap.*;
import java.io.IOException;

public class BaseRequest {

    private SOAPConnection soapConnection;

    public SOAPMessage sendSOAPRequest(SOAPMessage request, String soapEndpointUrl) {
        try{
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            soapConnection = soapConnectionFactory.createConnection();
            SOAPMessage response = soapConnection.call(request, soapEndpointUrl);
            System.out.println("Response SOAP Message:");
            response.writeTo(System.out);
            soapConnection.close();
            return response;
        } catch (SOAPException | IOException e) {
            throw new RuntimeException("Sending SOAP request failed");
        }
    }

    class RequestBuilder {

        public SOAPElement root;
        public SOAPEnvelope envelope;
        public SOAPMessage soapMessage;
        final String myNamespace = "lib";
        final String myNamespaceURI = "libraryService";

        public RequestBuilder(String rootName) {
            try {
                MessageFactory messageFactory = MessageFactory.newInstance();
                soapMessage = messageFactory.createMessage();
                init(rootName);
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
        }

        private void init(String localName) {
            try {
                SOAPPart soapPart = soapMessage.getSOAPPart();
                envelope = soapPart.getEnvelope();
                envelope.removeNamespaceDeclaration(envelope.getPrefix());
                envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);
                SOAPHeader header = soapMessage.getSOAPHeader();
                SOAPBody soapBody = envelope.getBody();
                Name rootName = envelope.createName(localName, myNamespace, myNamespaceURI);
                root = soapBody.addBodyElement(rootName);
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
        }

        RequestBuilder createAuthor(Author author) {
            try {
                SOAPElement authorElement = root.addChildElement("author", myNamespace);
                addAuthorId(author.getAuthorId(), authorElement);
                addAuthorName(author.getAuthorName().getFirst(), author.getAuthorName().getSecond(), authorElement);
                addAuthorNationality(author.getNationality(), authorElement);
                addAuthorBirth(author.getBirth().getDate(), author.getBirth().getCountry(), author.getBirth().getCity(),
                        authorElement);
                addAuthorDescription(author.getAuthorDescription(), authorElement);
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return this;
        }

        RequestBuilder deleteAuthor(int authorId, boolean forcibly) {
            try {
                addAuthorId(authorId);
                SOAPElement option = root.addChildElement("options", myNamespace);
                option.addChildElement("forcibly", myNamespace).setValue(String.valueOf(forcibly));
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }

            return this;
        }

        RequestBuilder addAuthorId(int authorId, SOAPElement element) {
            try {
                element.addChildElement("authorId", myNamespace).setValue(String.valueOf(authorId));
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return this;
        }

        RequestBuilder addAuthorId(int authorId) {
            try {
                root.addChildElement("authorId", myNamespace).setValue(String.valueOf(authorId));
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return this;
        }

        RequestBuilder addAuthorName(String first, String second, SOAPElement element) {
            try {
                SOAPElement name = element.addChildElement("authorName", myNamespace);
                name.addChildElement("first", myNamespace).setValue(first);
                name.addChildElement("second", myNamespace).setValue(second);
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return this;
        }

        RequestBuilder addAuthorNationality(String nationality, SOAPElement element) {
            try {
                element.addChildElement("nationality", myNamespace).setValue(nationality);
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return this;
        }

        RequestBuilder addAuthorBirth(String date, String  country, String city, SOAPElement element) {
            try {
                SOAPElement authorBirth = element.addChildElement("birth", myNamespace);
                authorBirth.addChildElement("date", myNamespace).setValue(date);
                authorBirth.addChildElement("country", myNamespace).setValue(country);
                authorBirth.addChildElement("city", myNamespace).setValue(city);
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return this;
        }

        RequestBuilder addAuthorDescription(String description, SOAPElement element) {
            try {
                element.addChildElement("description", myNamespace).setValue(description);
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return this;
        }

        public SOAPMessage build() {
            try {
                soapMessage.saveChanges();
            }
            catch (SOAPException e) {
                e.printStackTrace();
            }
            return soapMessage;
        }

    }


}
