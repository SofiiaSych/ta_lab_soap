package com.epam.client;

import com.epam.requests.AuthorRequest;
import com.epam.requests.BaseRequest;
import javax.xml.soap.*;
import java.util.concurrent.ThreadLocalRandom;


public class SoapClient {

    public static void main(String args[]){

        String soapEndpointUrl = "http://localhost:8080/ws";
        Integer authorId = ThreadLocalRandom.current().nextInt(2, 2100 + 1);

        AuthorRequest authorRequest = new AuthorRequest();
        SOAPMessage createAuthorRequest = authorRequest.createAuthorRequest(authorId);
        SOAPMessage getAuthorRequest = authorRequest.getAuthorRequest(authorId);
        SOAPMessage deleteAuthorRequest = authorRequest.deleteAuthorRequest(authorId);

        BaseRequest baseRequest = new BaseRequest();
        baseRequest.sendSOAPRequest(getAuthorRequest, soapEndpointUrl);
        baseRequest.sendSOAPRequest(createAuthorRequest, soapEndpointUrl);
        baseRequest.sendSOAPRequest(deleteAuthorRequest, soapEndpointUrl);

    }


}



