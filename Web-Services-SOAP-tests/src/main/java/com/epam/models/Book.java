package com.epam.models;

public class Book {
    private int bookId;
    private String bookName;
    private String bookLanguage;
    private String bookDescription;

    public static class Additional {
        private int pageCount;
        private Size size;

        public static class Size {
            private double height;
            private double width;
            private double length;

            public Size() {
            }

            public Size(double height, double width, double length) {
                this.height = height;
                this.width = width;
                this.length = length;
            }

            @Override
            public String toString() {
                return "Size{" +
                        "height=" + height +
                        ", width=" + width +
                        ", length=" + length +
                        '}';
            }
        }

        public Additional() {
        }

        public Additional(int pageCount, Size size) {
            this.pageCount = pageCount;
            this.size = size;
        }

        @Override
        public String toString() {
            return "Additional{" +
                    "pageCount=" + pageCount +
                    ", size=" + size +
                    '}';
        }
    }
}
