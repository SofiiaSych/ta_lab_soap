package com.epam.models;

public class Author {
    private int authorId;
    private Name authorName;
    private String nationality;
    private Birth birth;
    private String authorDescription;

    public Author() {

    }


    public static class Name {
        private String first;
        private String second;

        public Name(String first, String second) {
            this.first = first;
            this.second = second;
        }

        public String getFirst() {
            return first;
        }

        public String getSecond() {
            return second;
        }

        @Override
        public String toString() {
            return "Name{" +
                    "firstName='" + first + '\'' +
                    ", secondName='" + second + '\'' +
                    '}';
        }
    }

    public static class Birth {
        private String date;
        private String country;
        private String city;

        public Birth() {
        }

        public Birth(String date, String country, String city) {
            this.date = date;
            this.country = country;
            this.city = city;
        }

        public String getDate() {
            return date;
        }

        public String getCountry() {
            return country;
        }

        public String getCity() {
            return city;
        }

        @Override
        public String toString() {
            return "Birth{" +
                    "date=" + date +
                    ", country='" + country + '\'' +
                    ", city='" + city + '\'' +
                    '}';
        }
    }

    public Author(int authorId, Name authorName, String nationality, Birth birth, String authorDescription) {
        this.authorId = authorId;
        this.authorName = authorName;
        this.nationality = nationality;
        this.birth = birth;
        this.authorDescription = authorDescription;
    }

    public int getAuthorId() {
        return authorId;
    }

    public Name getAuthorName() {
        return authorName;
    }

    public String getNationality() {
        return nationality;
    }

    public Birth getBirth() {
        return birth;
    }

    public String getAuthorDescription() {
        return authorDescription;
    }

    @Override
    public String toString() {
        return "Author{" +
                "authorId=" + authorId +
                ", authorName=" + authorName +
                ", nationality='" + nationality + '\'' +
                ", birth=" + birth +
                ", authorDescription='" + authorDescription + '\'' +
                '}';
    }
}
