import com.epam.requests.AuthorRequest;
import com.epam.requests.BaseRequest;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.util.concurrent.ThreadLocalRandom;

public class AuthorTest {

    private AuthorRequest authorRequest;
    private BaseRequest baseRequest;
    private Integer authorId;
    public String soapEndpointUrl = "http://localhost:8080/ws";

    @BeforeMethod
    public void setUp() {
        authorRequest = new AuthorRequest();
        baseRequest = new BaseRequest();
        authorId = ThreadLocalRandom.current().nextInt(2, 2100 + 1);
    }

    @Test
    public void createAuthorTest() {
        try {
            SOAPMessage createAuthorRequest = authorRequest.createAuthorRequest(authorId);
            SOAPMessage response = baseRequest.sendSOAPRequest(createAuthorRequest, soapEndpointUrl);
            Assert.assertEquals(response.getSOAPBody().getElementsByTagName("ns2:first").item(0).getTextContent(),
                    "Arturo", "Author name is different as expected");
        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteAuthorTest() {
        try {
            SOAPMessage deleteAuthorRequest = authorRequest.deleteAuthorRequest(3);
            SOAPMessage response = baseRequest.sendSOAPRequest(deleteAuthorRequest, soapEndpointUrl);
            Assert.assertEquals(response.getSOAPBody().getElementsByTagName("ns2:status").item(0).getTextContent(),
                    "Successfully deleted author 3", "Delete failed");
        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getAuthorByIdTest() {
        try {
            SOAPMessage getAuthorByIdRequest = authorRequest.getAuthorRequest(310);
            SOAPMessage response = baseRequest.sendSOAPRequest(getAuthorByIdRequest, soapEndpointUrl);
            Assert.assertEquals(response.getSOAPBody().getElementsByTagName("ns2:authorId").item(0).getTextContent(),
                    "310", "Author id is different as expected");
        } catch (SOAPException e) {
            e.printStackTrace();
        }
    }
}

